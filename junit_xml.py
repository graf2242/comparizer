#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys

sys.path.append(os.getcwd())

import xml.etree.ElementTree
import datetime
import argparse
import hashlib
import random
import io


class TestSuite:
    def __init__(self, name='', accept_duplicates=False):
        # <testsuite name="TEST SUITE NAME" tests="TEST COUNT" time="0.0" failures="0" errors="0" skipped="0">
        self.name = name
        self.tests = []
        self.failures = 0
        self.tests_count = 0
        self.time = .0
        self.accept_duplicates = accept_duplicates
        self.originals = set()


class JunitXml:
    def __init__(self):
        # <testsuite name="TEST SUITE NAME" tests="TEST COUNT" time="0.0" failures="0" errors="0" skipped="0">
        self.suites = {}

    def test(self, suite_name='default', name='', failure_message='', short_message='', full_failure_message='', time=.0):
        # <testcase name="ASuccessfulTest"> <failure> message </failure> </testcase>
        if suite_name not in self.suites:
            self.suites[suite_name] = TestSuite(name=suite_name)
        test_suite = self.suites.get(suite_name)
        if test_suite.accept_duplicates or name + failure_message not in test_suite.originals:
            test_suite.originals.add(name + failure_message)
            test_suite.tests.append({
                'name': name,
                'failure_message': failure_message.encode(errors='xmlcharrefreplace').decode(),
                'short_message': short_message.encode(errors='xmlcharrefreplace').decode(),
                'full_failure_message': full_failure_message,
                'time': time})
            test_suite.tests_count += 1
            test_suite.failures += 1 if failure_message else 0
            test_suite.time += time

    def xml(self, file_=None):
        testsuites = xml.etree.ElementTree.Element('testsuites', attrib={
            'name': "AllTests",
            'tests': str(sum(suite.tests_count for suite in self.suites.values())),
            'failures': '1',
            'disabled': '1',
            'errors': '1',
            'timestamp': '1',
            'time': '0',
        })

        for suite_name, suite in self.suites.items():
            testsuite = xml.etree.ElementTree.Element('testsuite',
                                                      attrib={'name': suite.name,
                                                              'tests': str(suite.tests_count),
                                                              'time': str(suite.time),
                                                              'failures': str(suite.failures),
                                                              })

            for test in suite.tests:
                case = xml.etree.ElementTree.Element('testcase', attrib={
                    'name': test['name'],
                    'time': str(test['time'])
                })
                if test['failure_message']:
                    case = xml.etree.ElementTree.Element('testcase', attrib={
                        'time': str(test['time']),
                        'name': test['name']
                    })

                    failure = xml.etree.ElementTree.Element('failure', attrib={
                        'message': test['failure_message']
                    })
                    failure.text = test['full_failure_message']
                    case.append(failure)
                testsuite.append(case)
            testsuites.append(testsuite)

        # write xml
        target_file = file_ if file_ else io.BytesIO()
        e = xml.etree.ElementTree.ElementTree(testsuites)
        e.write(target_file, encoding="utf-8", xml_declaration=True)

        # return string if file not defined
        if not file_:
            target_file.seek(0)
            return target_file.readlines()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--random_tests', action='store', dest='random_tests', help='Write random tests to file',
                        default=None)
    parser.add_argument('-o', '--ok_test', action='store', dest='ok_test', help='Write OK test to file', default=None)
    options = parser.parse_args()

    junit = JunitXml('TestSuite')
    junit.test(name='stable.ok', time=random.random() * 8)

    if options.random_tests:
        junit.test(name='stable.fail', failure_message='OH SHI', time=random.random() * 8)
        for item in range(int(random.random() * 16)):
            failure_message = ''
            if random.choice([True, False]):
                for stacktrace in range(int(random.random() * 32)):
                    line = hashlib.md5(str(stacktrace).encode()).hexdigest()
                    line = line[0:int(random.random() * len(line))]
                    failure_message += line + '\n'

            junit.test(name=hashlib.md5(str(item).encode()).hexdigest(),
                       failure_message=failure_message,
                       time=random.random() * 8)
        with open(options.random_tests, 'wb') as output_file:
            junit.xml(output_file)

    elif options.ok_test:
        with open(options.ok_test, 'wb') as output_file:
            junit.xml(output_file)

    print(junit.xml())


if __name__ == '__main__':
    main()
